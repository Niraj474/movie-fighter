const API_KEY = "c5d934e6";

// function test(value) {
//     console.log("HI");
//     console.log(arguments); //arguments object which only works in function declaration.//arguments object is not an array it only looks like an array.//it returns parameters passed to a function.
//     // arguments array have an custom iterator.
//     for (let value of arguments) {
//         console.log(value);
//     }
// }

// createAutoComplete({
//     root: document.querySelector('.autocomplete'),
//     renderOption(obj) {
//         return `
//             <img src="${obj.thumbnailUrl}"/>
//             ${obj.title}
//         `;
//     },
//     onOptionSelect(obj) {
//         getMovie(obj);
//     },
//     inputValue(obj) {
//         return obj.Title;
//     },
//     async fetchData(searchText) {
//         const response = await axios.get('https://jsonplaceholder.typicode.com/photos?albumId=1', {
//             // params: {
//             //     // apikey: API_KEY,
//             //     s: searchText,
//             // }
//         });
//         console.log(response);

//         return response.data;

//     }

// }); just for testing purpose of reusability of autoComplete.

const autoCompleteConfig = {
    renderOption(movie) {
        const imgSrc = movie.Poster == "N/A" ? "" : movie.Poster;
        return `
        <img src="${imgSrc}">
        ${movie.Title} (${movie.Year})
        `;
    },
    inputValue: movie => movie.Title,
    fetchData: async searchText => {
        const response = await axios.get("http://www.omdbapi.com/", {
            params: {
                apikey: API_KEY,
                s: searchText,
            },
        });

        if (response.data.Error) {
            return [];
        }

        return response.data.Search;
    }
}

let leftMovie, rightMovie;

createAutoComplete({
    ...autoCompleteConfig,
    root: document.getElementById('left-autocomplete'),
    onMovieSelect(movie) {
        document.querySelector('.tutorial').classList.add('is-hidden');
        getMovie(movie, document.getElementById('left-summary'), 'left');
    },

});
createAutoComplete({
    ...autoCompleteConfig,
    root: document.getElementById('right-autocomplete'),

    onMovieSelect(movie) {
        document.querySelector('.tutorial').classList.add('is-hidden');
        getMovie(movie, document.getElementById('right-summary'), 'right');
    },
});

const getMovie = async (movie, summaryElement, side) => {
    const response = await axios.get("http://www.omdbapi.com/", {
        params: {
            apikey: API_KEY,
            i: movie.imdbID
        }
    });

    summaryElement.innerHTML = renderMovieDetails(response.data);

    if (side == 'left')
        leftMovie = movie;
    else
        rightMovie = movie;

    if (leftMovie && rightMovie)
        compareMovies();
}

const toggleClasses = (greaterElement, smallerElement) => {
    smallerElement.classList.add('is-warning');
    smallerElement.classList.remove('is-primary');
    greaterElement.classList.add('is-primary');
    greaterElement.classList.remove('is-warning');
}

const compareMovies = () => {
    const leftSideStats = document.querySelectorAll('#left-summary .notification');
    const rightSideStats = document.querySelectorAll('#right-summary .notification');

    updateMovieStyles(leftSideStats, rightSideStats);

}

const updateMovieStyles = (leftSideStats, rightSideStats) => {

    leftSideStats.forEach((leftSideStat, index) => {
        const rightSideStat = rightSideStats[index];
        const leftSideValue = parseInt(leftSideStat.dataset.value);
        const rightSideValue = parseInt(rightSideStat.dataset.value);

        leftSideStat.classList.remove('is-warning');
        rightSideStat.classList.remove('is-warning');

        if (isNaN(leftSideValue) && isNaN(rightSideValue)) {
            leftSideStat.classList.add('is-primary');
            rightSideStat.classList.add('is-primary');
            return;

        }

        if (leftSideValue > rightSideValue && (!isNaN(leftSideValue)))
            toggleClasses(leftSideStat, rightSideStat);
        else {
            if ((!isNaN(rightSideValue)))
                toggleClasses(rightSideStat, leftSideStat);
            else
                toggleClasses(leftSideStat, rightSideStat);
        }
    });
}

const fetchMovieDetails = movieDetails => {
    return {
        boxOffice: parseInt(movieDetails.BoxOffice.replace(/\$/, '').replace(/,/g, '')), //g is used so that all , should be romoved if g isn't there then only first ',' will get removed.
        metascore: parseInt(movieDetails.Metascore),
        rating: parseFloat(movieDetails.imdbRating),
        votes: parseInt(movieDetails.imdbVotes.replace(/,/g, '')),
        imgPoster: movieDetails.Poster == "N/A" ? '' : movieDetails.Poster,
        awards: movieDetails.Awards.split(' ').reduce((prev, word) => {
            const value = parseInt(word);


            if (isNaN(value))
                return prev;
            else
                return prev + value;
        }, 0)
    };
}

const renderMovieDetails = movieDetail => {

    const {
        boxOffice,
        metascore,
        rating,
        votes,
        imgPoster,
        awards
    } = fetchMovieDetails(movieDetail);

    return `
        <article class="media">
            <figure class="media-left">
                <p class="image">
                    <img src="${imgPoster}"/>
                </p>
            </figure>
            <div class="media-content">
                <div class="content">
                    <h1>${movieDetail.Title}</h1>
                    <h4>${movieDetail.Genre}</h4>
                    <p>${movieDetail.Plot}</p>
                </div>
            </div>
        </article>
        <article data-value=${awards} class="notification is-primary">
            <p class="title">${movieDetail.Awards}</p>
            <p class="subtitle">Awards</p>
        </article>
        <article  data-value=${boxOffice} class="notification is-primary">
            <p class="title">${movieDetail.BoxOffice}</p>
            <p class="subtitle">Box Office</p>
        </article>
        <article  data-value=${metascore} class="notification is-primary">
            <p class="title">${movieDetail.Metascore}</p>
            <p class="subtitle">Metascore</p>
        </article>
        <article  data-value=${rating} class="notification is-primary">
            <p class="title">${movieDetail.imdbRating}</p>
            <p class="subtitle">IMDB Rating</p>
        </article>
        <article  data-value=${votes} class="notification is-primary">
            <p class="title">${movieDetail.imdbVotes}</p>
            <p class="subtitle">IMDB Votes</p>
        </article>

    `;
}