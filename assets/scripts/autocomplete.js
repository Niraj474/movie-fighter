//createAutoComplete is a reusable function which can be used in any other projects
// we just have to pass a configuration object.

const createAutoComplete = ({
    root,
    renderOption,
    onMovieSelect,
    inputValue,
    fetchData
}) => {
    root.innerHTML = `
        <label><b>Search For a Movie</b></label>
        <input class="input"/>
        <div class="dropdown">
        <div class="dropdown-menu">
        <div class="dropdown-content results"></div>
        </div>
        </div>
    `;
    const onInput = async (event) => {
        const items = await fetchData(event.target.value);
        if (!items.length) {
            dropdown.classList.remove("is-active");
            return;
        }

        resultsWrapper.innerHTML = "";
        dropdown.classList.add("is-active");
        for (let item of items) {
            const option = document.createElement("a");

            option.innerHTML = renderOption(item);

            option.classList.add("dropdown-item");
            option.addEventListener('click', event => {
                dropdown.classList.remove("is-active");
                input.value = inputValue(item);
                onMovieSelect(item);
            });
            resultsWrapper.appendChild(option);
        }
    };

    const dropdown = root.querySelector(".dropdown");
    const resultsWrapper = root.querySelector(".results");
    const input = root.querySelector("input");
    input.addEventListener("input", debounce(onInput, 1000)); //here debounce will return a function which is called by event listener whenever input is changed and in debounce we have applied args which is passed arguments(i.e. event object) from event listener

    document.addEventListener("click", (event) => {
        if (!root.contains(event.target)) {
            dropdown.classList.remove("is-active");
        }
    });


}