const debounce = (func, delay) => {
    let timeoutId;
    return (args) => { // this is rest parameters which only works in function expression.
        // ...args returns an array of all the parameters passed to that function.
        // Refer https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/rest_parameters
        // console.log(args);
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
        timeoutId = setTimeout(() => {
            func.call(null, args);
            // func.apply(null, args);if in some case ...args is used.
        }, delay);
    };

};